# Bitmap Font Cookie Cutter

This is a [cookiecutter](https://github.com/audreyr/cookiecutter) template to
generate bitmap fonts from the BMF font format.

Read more about how this works in the [Type:Bits
documentation](https://typebits.gitlab.io/new-font/#method-2-use-the-cookiecutter-template).

This template was created for the [Type:Bits](https://typebits.gitlab.io)
series of workshops by [Manufactura Independente](http://manufacturaindependente.org).

## Running

No need to install anything from this repository; just install `cookiecutter` and do:

    cookiecutter https://gitlab.com/typebits/cookiecutter-bitmapfont

After filling out some of the font's details, you'll have a new empty font --
time to fill out the glyphs!

Once you're done, read the [building
instructions](https://typebits.gitlab.io/building/) in order to create the font
files for use in your graphics software.

