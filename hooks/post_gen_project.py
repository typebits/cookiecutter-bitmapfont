#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
cwd = os.getcwd()
glyph_list = open('glyphs.txt', 'r').readlines()

# create an empty file for each glyph
os.makedirs('glyphs')
for filename in glyph_list:
    path = "glyphs/" + filename.strip()
    open(path, 'a').close()

# finally, remove the glyph list
os.remove('glyphs.txt')
