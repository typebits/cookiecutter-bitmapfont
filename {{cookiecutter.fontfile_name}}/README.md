# {{ cookiecutter.font_name }}

This is the source repository for the {{ cookiecutter.font_name }} font.

![Font preview]({{cookiecutter.git_repository_url/-/jobs/artifacts/master/raw/{{cookiecutter.fontfile_name}}-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-skeleton/)

Download formats:

* [TrueType (.ttf)]({{cookiecutter.git_repository_url/-/jobs/artifacts/master/raw/{{cookiecutter.fontfile_name}}-Regular.ttf?job=build-font)
* [OpenType (.otf)]({{cookiecutter.git_repository_url/-/jobs/artifacts/master/raw/{{cookiecutter.fontfile_name}}-Regular.otf?job=build-font)
* [FontForge (.sfd)]({{cookiecutter.git_repository_url/-/jobs/artifacts/master/raw/{{cookiecutter.fontfile_name}}-Regular.sfd?job=build-font)

## Authors

{{ cookiecutter.authors }}

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
