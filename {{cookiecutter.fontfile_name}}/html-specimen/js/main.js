$(document).ready(function(){
    $(".fonttester input").bind('keypress keyup blur', function() {
        if ($(this).val() == '') {
            $(".sample").text('The quick brown fox jumps over the lazy dog');       
        }
        else {
            $(".sample").text($(this).val());
        } 
    });
});
